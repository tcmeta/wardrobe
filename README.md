[Version]:0
[Grid]:button
[Comment]:Initial%20Script!
[Url]:https://bitbucket.org/thecollectivesl/wardrobe
[end]:end
![Wardrobe Banner](https://bitbucket.org/thecollectivesl/wardrobe/raw/master/img/Wardrobe.png)
# High Fidelity Wardrobe System #

This is a wardrobe system that allows a user to store and change premade outfits as well as make a central api to allow other apps to save and edit outfits as well.

### Features ###

* Tablet app that allows for users to easily store, change and remove outfits.
* A simple api that will allow people to save, change and wear outfits without having to switch apps.
* Import and Export your wardrobe

### API ###

The api works by using the EventBridge.emitWebEvent() function within your tablet app. The calls are sent in a JSON format that is stringified at trigger.

Example
``` 
EventBridge.emitWebEvent(JSON.Stringify({
	app: "TC_Wardrobe",
	cmd: "getVersion",
	value: {}
}));
```
The Returned json will be in the following format after JSON.Parse()
```
{
	app: "TC_Wardrobe",
	cmd: "getVersion",
	value: {
		version: 1
	}
}
```
Using this formatting you can pass through variables as well that that you may want to use as a identifier for the api call.

The following commands are supported to this script.

* getVersion		{app: "TC_Wardrobe", cmd: "getVersion", value: {}} 											returns {app: "TC_Wardrobe", cmd: "getVersion", value: {version: versionNumber}}
* listSettings		{app: "TC_Wardrobe", cmd: "listSettings", value: {}}										returns {app: "TC_Wardrobe", cmd: "listSettings", value: {settings: [{}]}}
* getSetting		{app: "TC_Wardrobe", cmd: "getSetting", value: {name: name}}								returns {app: "TC_Wardrobe", cmd: "getSetting", value: {name: name, settings:{all settings}}} / {app: "TC_Wardrobe", cmd: "getSetting", value: {name: name, settings:{undefined: true}}}
* setSetting		{app: "TC_Wardrobe", cmd: "setSetting", value: {name: name, settings: {setting: value}}}	returns {app: "TC_Wardrobe", cmd: "setSetting", value: {name: name, settings: {setting: value}}}
* listOutfits		{app: "TC_Wardrobe", cmd: "listOutfits", value: {index: indexnumber, listLength: ammount}}	returns {app: "TC_Wardrobe", cmd: "listOutfits", value: {outfit: [{name: outfitname}], index: indexnumber, listLength: ammount, total: totaloutfits}}
* getOutfit			{app: "TC_Wardrobe", cmd: "getOutfit", value: {index: index}}								returns {app: "TC_Wardrobe", cmd: "getOutfit", value: {index: index, outfit: {outfitobject}}}
* setOutfit		{app: "TC_Wardrobe", cmd: "setOutfit", value: {index: index, settings: {setting: value}}}		returns {app: "TC_Wardrobe", cmd: "setOutfit", value: {index: index, settings: {setting: value}}, updated: true/false}
* wearOutfit		{app: "TC_Wardrobe", cmd: "wearOutfit", value: {index: index}}								returns {app: "TC_Wardrobe", cmd: "wearOutfit", value: {index: index, worn: true/false}}
* saveOutfit		{app: "TC_Wardrobe", cmd: "saveOutfit", value: {name: name}}								returns
* deleteOutfit		{app: "TC_Wardrobe", cmd: "deleteOutfit", value: {index: index}}							returns
* updateOutfit		{app: "TC_Wardrobe", cmd: "updateOutfit", value: {index: index}}							returns {app: "TC_Wardrobe", cmd: "updateOutfit", value: {index: index, saved: true/false}}
* exportWardrobe	{app: "TC_Wardrobe", cmd: "exportWardrobe", value: {}}										returns {app: "TC_Wardrobe", cmd: "exportWardrobe", value: {wardrobe: "string of json wardrobe"}} 
* importWardrobe	{app: "TC_Wardrobe", cmd: "importWardrobe", value: {wardobe: wardrobe object (parsed)}}		returns	{app: "TC_Wardrobe", cmd: "importWardrobe", value: {wardrobe: wardrobe object (parsed), applied: true/false}} 
* clearWardrobe		{app: "TC_Wardrobe", cmd: "clearWardrobe", value: {}}										returns {app: "TC_Wardrobe", cmd: "clearWardrobe", value: {success: true/false}}
* setSkeletonModeURL {this command is not accepted by the client}												returns {app: "TC_Wardrobe", cmd: "setSkeletonModeURL", value: {url: url}}

The setSkeletonModeURL may be returned and should be considered if you are planning to use certain parts of the api

>If applying a new outfit, this call may be returned if the MyAvatar.SkeletonModelURL is not the same as the oufits. This can be simply dealt with the following when parsed into a variable. 
>```
>EventBridge.scriptEventReceived.connect(function(message) {
>	var message = JSON.parse(message);
>	if (message.cmd == "setSkeletonModelURL") {
>		window.location = message.value;
>	}
>});
>```


### Who do I talk to? ###

* [Kurtis Anatine](https://keybase.io/theguywho)