/*
    Wardrobe App. Made by Kurtis Anatine (Vargink)
    Get in contact with me via https://keybase.io/theguywho 
    If they can't back with a hash then you better dash!

    Get the latest for this and documentation at https://bitbucket.org/thecollectivesl/wardrobe

    This work is licenced to the public under the Creative Commons Attribution 4.0 International (CC BY 4.0) https://creativecommons.org/licenses/by/4.0/ formally known as AYDMD
    by myself. This works comes with no warranty

*/
(function() {
    // Global Objects
    var self = {
        app: "TC_Wardrobe",
        version: "1",
        debug: true,
        timerCheck: null,
        urlIndex: Script.resolvePath("html/wardrobe.html"),
        iconIndex: Script.resolvePath("icons/wardrobe.svg")
    };
    var tablet = Tablet.getTablet("com.highfidelity.interface.tablet.system");
    var button = tablet.addButton({
        text: "WARDROBE",
        icon: self.iconIndex,
        activeIcon: self.iconIndex
    });
    var wardrobe;
    // Debuging
    function debug(message) {
        if (self.debug) {
            print(message);
        }
    }
    // On Closing
    Script.scriptEnding.connect(function() {
        if (self.timerCheck !== null) {
            Script.clearInterval(self.timerCheck);
            self.timerCheck = null;
        }
        tablet.removeButton(button);
    });

    function trimURL(url) {
        url = url.substring(0, (url.indexOf("?") === -1) ? url.length : url.indexOf("?"));
        return url;
    }

    function removeWard(url) {
        // this is to remove any trailing #ward values if you resave your attachments.
        url = url.substring(0, (url.indexOf("#ward") === -1) ? url.length : url.indexOf("#ward"));
        return url;
    }

    function findOutfitIndex(outfitName) {
        var i = 0;
        for (i=0; i < wardrobe.outfit.length; i++) {
            if (wardrobe.outfit[i].name === outfitName) {
                break;
            }
        }
        return i < wardrobe.outfit.length ? i : -1;
    }

    function saveWardrobe() {
        // lets sort all the outfits so they are in alphabetical order. We would only need to do this when the wardrobe is being saved.
        wardrobe.outfit.sort(function(a, b) {
            var nameA = a.name.toLowerCase();
            var nameB = b.name.toLowerCase();
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            return 0;
        });
        Settings.setValue(self.app, wardrobe);
    }

    function loadWardrobe() {
        wardrobe = Settings.getValue(self.app); 
        if (typeof(wardrobe) === 'string') {
            // if the settings ini does not exist. Create a blank object. Might as well save it too for next time.
            if (wardrobe === "") {
                wardrobe = {
                    outfit: [],
                    version: 1,
                    settings: {}
                };
            } else {
                // This would be someone who is using the original version of the wardrobe. Parse and update to the new version and then save it.
                wardrobe = JSON.parse(wardrobe);
            }
            saveWardrobe();
        }
        // lets make a nice little version updater!
        if (typeof(wardrobe.version) === 'undefined') {
            // Initial update for the versioning and also putting a settings store here as well. I think it might be a good idea to leave the settings area opened up.
            // Other apps could store variables here if they wanted to. Think i might just allow for blind additions here as well. Make a set return call if there is nothing
            // apps could just use that as a standard "put default here untill we set it" and not have to worry about sanity checks on their end.
            wardrobe.version = 1;
            wardrobe.settings = {};
            saveWardrobe();
        }
        // The idea here is to place sequential updates for the object here. If anything major is added or changed we can just use version comparison to rebuild this with the previous version.
        // Lets do it the REKON Accounts / Quickbooks way. At least with this its fully automated.
        if (wardrobe.version === 1) {
            // added scale option to the menu to allow for scale to not apply with outfits.
            wardrobe.version = 2;
            wardrobe.settings.setScale = {enabled: true};
            saveWardrobe();
        }
        if (wardrobe.version === 2) {
            // added a bypass check for avatar base
            wardrobe.version = 3;
            wardrobe.settings.changeSkeletonModelURL = {enabled: true};
        }
    }

    function buildOutfit(name) {
        debug("saving avatar to object");
        var attachments = MyAvatar.getAttachmentData();
        var curAvatar = {
            name: name,
            skeletonModelURL: MyAvatar.skeletonModelURL, 
            scale: MyAvatar.scale,
            attachments: []
        };
        for (var i = 0; i < attachments.length; i++) {
            curAvatar.attachments.push({
                "modelURL": removeWard(attachments[i].modelURL),
                "jointName": attachments[i].jointName,
                "translation": attachments[i].translation,
                "rotation": attachments[i].rotation,
                "scale": attachments[i].scale,
                "isSoft": attachments[i].isSoft
            });
        }
        return curAvatar;
    }

    function putOn(event) {
        if (wardrobe.settings.setScale.enabled) {
            MyAvatar.scale = wardrobe.outfit[event.value.index].scale;
        }
        var attachments = MyAvatar.getAttachmentData();
        for (var i = 0; i < attachments.length; i++) {
            MyAvatar.detachOne(attachments[i].modelURL);
        }
        for (i = 0; i < wardrobe.outfit[event.value.index].attachments.length; i++) {
            // okay so there is an issue with multiple attachments with the same url. My plan is to add a #identifier since this should not cache stomp every version
            // of the attachment.
            MyAvatar.attach(
                wardrobe.outfit[event.value.index].attachments[i].modelURL+"#ward"+i,
                wardrobe.outfit[event.value.index].attachments[i].jointName,
                wardrobe.outfit[event.value.index].attachments[i].translation,
                wardrobe.outfit[event.value.index].attachments[i].rotation,
                wardrobe.outfit[event.value.index].attachments[i].scale,
                wardrobe.outfit[event.value.index].attachments[i].isSoft
            );
        }
        event.value.worn = true;
        tablet.emitScriptEvent(JSON.stringify(event));
    }

    function webEvent(event) {
        if (typeof(event) === "string") {
            debug("event string:"+event);
            // Should stop anything funny going on since other events can be grabbed
            try {
                event = JSON.parse(event);
                if (event.app === self.app) {
                    switch (event.cmd) {
                        case "getVersion":
                            event.value.version = self.version;
                            tablet.emitScriptEvent(JSON.stringify(event));
                            break;
                        case "getSetting":
                            // need to change this to work with the change of the settings object structure
                            if (typeof(wardrobe.settings[event.value.name]) === 'undefined') {
                                event.value.settings = {undefined: true};
                            } else {
                                event.value.settings = wardrobe.settings[event.value.name];
                            }
                            tablet.emitScriptEvent(JSON.stringify(event));
                            break;
                        case "setOutfit":
                            // we are just going to use this for names at the moment, this could be made to edit other stuff but i dont see a point since well, update works
                            if (typeof(wardrobe.outfit[event.value.index]) === 'object') {
                                for (var settings in event.value.settings) {
                                    if (settings === "name") {
                                        var tempindex = findOutfitIndex(event.value.settings[settings]);
                                        while (tempindex !== -1) {
                                            event.value.settings[settings] += " (new)";
                                            tempindex = findOutfitIndex(event.value.settings[settings]);
                                        }
                                        wardrobe.outfit[event.value.index][settings] = event.value.settings[settings];
                                    }
                                }
                                event.value.updated = true;
                                tablet.emitScriptEvent(JSON.stringify(event));
                            }
                            break;
                        case "setSetting":
                            // need to redo this for changed in the object structure
                            if (typeof(wardrobe.settings[event.value.name]) === 'undefined') {
                                wardrobe.settings[event.value.name] = {};
                            }
                            // now that we know for certain this exists, lets dump data!
                            for (settings in event.value.settings) {
                                wardrobe.settings[event.value.name][settings] = event.value.settings[settings];
                            }
                            saveWardrobe();
                            // send a "yeah we did it reply"
                            event.value.settings.applied = true;
                            tablet.emitScriptEvent(JSON.stringify(event));
                            break;
                        case "getOutfit":
                            if (typeof(wardrobe.outfit[event.value.index]) === 'undefined') {
                                event.value.outfit = {undefined: true};
                            } else {
                                event.value.outfit = wardrobe.outfit[event.value.index];
                            }
                            tablet.emitScriptEvent(JSON.stringify(event));
                            break;
                        case "listOutfits":
                            debug("Initial Index: "+event.value.index);
                            if (event.value.index === -1) {
                                event.value.index = Math.floor(wardrobe.outfit.length / event.value.listLength);
                                if (event.value.index === wardrobe.outfit.length / event.value.listLength) {
                                    event.value.index--;
                                }
                            } else if (event.value.index >= Math.ceil(wardrobe.outfit.length / event.value.listLength)) {
                                event.value.index = 0;
                            }
                            debug("Updated Index is: "+event.value.index);
                            event.value.outfit = [];
                            for (var i = event.value.index * event.value.listLength; i < wardrobe.outfit.length && i < (event.value.index * event.value.listLength) + event.value.listLength;
                                i++) {
                                event.value.outfit.push({
                                    name: wardrobe.outfit[i].name
                                });
                            }
                            // add in the total ammount of outfits in the wardrobe (honestly putting this in so that i can check if i need a forward or back button and 
                            // i wouldnt need to do a 1 -1 call to get a total index)
                            event.value.total = wardrobe.outfit.length;
                            tablet.emitScriptEvent(JSON.stringify(event));
                            break;
                        case "saveOutfit":
                            event.value.index = findOutfitIndex(event.value.name);
                            while (event.value.index !== -1) {
                                event.value.name += " (new)";
                                event.value.index = findOutfitIndex(event.value.name);
                            }
                            var newOutfit = buildOutfit(event.value.name);
                            wardrobe.outfit.push(newOutfit);
                            saveWardrobe();
                            event.value.index = findOutfitIndex(event.value.name);
                            tablet.emitScriptEvent(JSON.stringify(event));
                            break;
                        case "updateOutfit":
                            newOutfit = buildOutfit(wardrobe.outfit[event.value.index].name);
                            wardrobe.outfit[event.value.index] = newOutfit;
                            saveWardrobe();
                            event.value.saved = true;
                            tablet.emitScriptEvent(JSON.stringify(event));
                            break;
                        case "deleteOutfit":
                            wardrobe.outfit.splice(event.value.index,1);
                            saveWardrobe();
                            event.value.index = -1;
                            tablet.emitScriptEvent(JSON.stringify(event));
                            break;
                        case "wearOutfit":
                            // Note: attach when putting the same asset on at the same spot updates the avatar. You will need to append a 
                            // #number or something to the end to make the strings different
                            if (self.timerCheck !== null) {
                                Script.clearInterval(self.timerCheck);
                                self.timerCheck = null;
                            }
                            debug("putting on outfit with id: "+event.value.index);
                            if (trimURL(MyAvatar.skeletonModelURL) === trimURL(wardrobe.outfit[event.value.index].skeletonModelURL) || !wardrobe.settings.changeSkeletonModelURL.enabled) {
                                putOn(event);
                            } else {
                                // we send back to the app to redirect to the baseurl. They will get a prompt and after thats done we can fire this again!
                                //tablet.emitScriptEvent(JSON.stringify({app: self.app, cmd: "setSkeletonModelURL", value: {url: wardrobe.outfit[event.value.index].skeletonModelURL}}));
                                MyAvatar.skeletonModelURL = wardrobe.outfit[event.value.index].skeletonModelURL;
                                debug("Model URL:"+wardrobe.outfit[event.value.index].skeletonModelURL);
                                self.timerCheck = Script.setInterval(function() {
                                    // Now we need to wait for the base skeleton to be put on and then we can just pass this through again. Should just work and send the reply
                                    // oh boy self calling functions hnng.
                                    debug("timer trigger");
                                    if (trimURL(MyAvatar.skeletonModelURL) === trimURL(wardrobe.outfit[event.value.index].skeletonModelURL)) {
                                        Script.clearInterval(self.timerCheck);
                                        self.timerCheck = null;
                                        putOn(event);
                                    } else {
                                        // avatar might change so lets just try and enforce it again.
                                        MyAvatar.skeletonModelURL = wardrobe.outfit[event.value.index].skeletonModelURL;
                                    }
                                }, 2000);
                            }
                            break;
                        case "exportWardrobe":
                            event.value.wardrobe = wardrobe;
                            tablet.emitScriptEvent(JSON.stringify(event));
                            break;
                        case "importWardrobe":
                            try {
                                wardrobe = event.value.wardrobe;
                                saveWardrobe();
                                event.value.applied = true;
                            } catch (err) {
                                debug("import failed reason: "+err.message);
                                event.value.applied = false;
                            }
                            tablet.emitScriptEvent(JSON.stringify(event));
                            break;
                        case "clearWardrobe":
                            Settings.setValue(self.app, "");
                            loadWardrobe();
                            event.value.success = true;
                            tablet.emitScriptEvent(JSON.stringify(event));
                            break;
                    }
                }
            } catch (err) {
                debug("Error parsing message : "+err.message);
            }
        }
    }

    function onClicked() {
        // Just in case the user has hit no to changing the avatar and they have since then hit the app button to bring it back up.
        if (self.timerCheck !== null) {
            Script.clearInterval(self.timerCheck);
            self.timerCheck = null;
        }
        tablet.gotoWebScreen(self.urlIndex);
    }

    // Init stuff
    function start() {
        // load the wardrobe up on start!
        loadWardrobe();
        // make stuff smash really good
        tablet.webEventReceived.connect(webEvent);
        // make dat like button work yo
        button.clicked.connect(onClicked);
        debug("Loaded");
    }

    // Starting Part
    start();
}());